#!/bin/bash
if [ "all" = $1 ]; then
	# evaluate the predictions
	sh all_java.sh tokenize
	sh all_java.sh estimate
	sh all_java.sh predict
	sh all_java.sh evaluate
else
    sh build_java.sh $1
    sh run_java.sh $1
fi