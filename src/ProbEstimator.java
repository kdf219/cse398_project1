import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class ProbEstimator {
    static int V = 0, N = 0;
    static String separator = "kelli";

    public static void main(String[] args) throws IOException {
        Set<String> vocab = new HashSet<>();

        // add words to the vocab
        vocab = addToSet(vocab, args[0]);

        if (args.length > 1)
            vocab = addToSet(vocab, args[1]);

        V = vocab.size(); // 25864 for testing

        // calculate bigram frequencies
        HashMap<String, Integer> bigramFrequencies = getBigramFrequencies(args[0]);

        // write all bigrams and frequencies to results/bigrams.txt
        PrintWriter pw = new PrintWriter(new File("results/bigrams.txt"));
        StringBuilder sb = new StringBuilder();
        StringBuilder finalSb1 = sb;

        // put V and N values at top of file for easy access
        finalSb1.append(V).append("\n");
        finalSb1.append(N).append("\n");

        // print bigrams and their frequencies to bigrams.txt
        bigramFrequencies.forEach((key, value) -> {
            finalSb1.append(key).append("->").append(value).append("\n");
        });
        pw.write(sb.toString());
        pw.close();

        // Get frequency of n0 and n1
//        int n0Tokens = calculateFrequency(0, bigramFrequencies); // V * (V - 1) - uniqueBigrams;
//        int n1Tokens = calculateFrequency(1, bigramFrequencies);

        // get the frequency of frequencies of bigrams

        HashMap<Integer, Integer> ff = new HashMap<>();
        bigramFrequencies.forEach((key, value) -> {
            ff.merge(value, 1, (a, b) -> a + b);
        });

        // print frequency of frequencies to ff.txt
        // commented out because we don't use this file for anything
//        pw = new PrintWriter(new File("results/ff.txt"));
//        sb = new StringBuilder();
//        StringBuilder finalSb = sb;
//        ff.forEach((key, value) -> {
//            finalSb.append(Math.max(Math.log(key), 0)).append(",")
//                    .append(Math.max(0, Math.log(value))).append("\n");
//        });
//        pw.write(sb.toString());
//        pw.close();


        // calculate gt values for c 0-5
        // Commented out because the file is never used

//        pw = new PrintWriter(new File("results/GTTable.txt"));
//        sb = new StringBuilder();
//
//        // calculate and output GT smooth predicted count for the 0-5
//        for (int c = 0; c < 6; c++) {
//            try {
//                sb.append(c).append("...").append((c + 1) * (1.0 * ff.get(c + 1) / ff.get(c))).append("\n");
//            } catch (NullPointerException e) {
//                sb.append((c + 1) * (1.0 * ff.get(1) / N)).append("\n");
//            }
//        }
//
//        pw.write(sb.toString());
//        pw.close();


    }


    static Set<String> addToSet(Set<String> vocab, String fileName) throws IOException {
        BufferedReader br = new BufferedReader(new FileReader(fileName));
        // adds all unique words from first file
        String line = null;
        while ((line = br.readLine()) != null) {
            if (!vocab.contains(line))
                V++;
            N++;
            vocab.add(line);
        }
        return vocab;
    }

    static HashMap<String, Integer> getBigramFrequencies(String trainingFile) throws IOException {
        HashMap<String, Integer> bigrams = new HashMap<>();

        BufferedReader br = new BufferedReader(new FileReader(trainingFile));
        String first = br.readLine();
        String second;
        while ((second = br.readLine()) != null) {
            String bigram = first + separator + second;
            bigrams.merge(bigram, 1, (a, b) -> a + b);
            first = second;
        }
        return bigrams;
    }


    // Get Nc from a hashmap of bigrams and their frequencies for any c
//    static int calculateFrequency(int c, HashMap<String, Integer> bigramFrequencies) {
//        // use equation if zero, otherwise increment as counting through
//        if (c == 0) {
//            return V * (V - 1) - bigramFrequencies.size();
//        }
//        AtomicInteger nc = new AtomicInteger();
//        bigramFrequencies.forEach((key, value) -> {
//            if (value == c)
//                nc.getAndIncrement();
//        });
//        return nc.get();
//    }
}
