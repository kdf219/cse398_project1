import java.io.*;
import java.util.HashMap;

public class Predictor {
    public static void main(String[] args) throws IOException {
        String separator = "kelli";

        HashMap<String, Integer> bigramFrequencies = new HashMap<>();

        BufferedReader br = new BufferedReader(new FileReader("results/bigrams.txt")); // train_tokens.txt

        // Get V and N values from first two lines of bigrams.txt file
        int V = Integer.parseInt(br.readLine());
        int N = Integer.parseInt(br.readLine());

        String curLine;

        // parses bigrams.txt file and puts bigram, frequency into hashmap
        while ((curLine = br.readLine()) != null) {
            String arrow = "->";
            bigramFrequencies.put(curLine.substring(0, curLine.indexOf(arrow)),
                    Integer.parseInt(curLine.substring(curLine.indexOf(arrow) + arrow.length())));
        }

        // puts all confusing words into a hashmap both ways
        HashMap<String, String> confusingWords = new HashMap<>();
        br = new BufferedReader(new FileReader("data/all_confusingWords.txt"));
        while ((curLine = br.readLine()) != null) {
            String key1 = curLine.substring(0, curLine.indexOf(":"));
            confusingWords.put(key1, curLine.substring(curLine.indexOf(":") + 1));
            confusingWords.put(confusingWords.get(key1), key1);
        }


        // getting values from gttable and putting into hashmap
        // Commented out because we don't do anything with the values from the table
        
        HashMap<Integer, Double> GTVals = new HashMap<>();
        br = new BufferedReader(new FileReader("results/GTTable.txt"));
        while ((curLine = br.readLine()) != null) {
            String mid = "...";
            GTVals.put(Integer.parseInt((curLine.substring(0, curLine.indexOf(mid)))),
                    Double.parseDouble(curLine.substring(curLine.indexOf(mid) + mid.length())));
        }
        

	HashMap<Integer, Double> ff = new HashMap<>();
        br = new BufferedReader(new FileReader("results/ff.txt"));
        while ((curLine = br.readLine()) != null) {
            ff.put(Math.log(curLine.substring(0,curLine.indexOf(",")));
        }



        // iterate through test file and look for confusing words
        br = new BufferedReader(new FileReader(args[0])); // test_tokens_fake.txt
        PrintWriter pw = new PrintWriter(new File(args[1]));
        StringBuilder sb = new StringBuilder();

        int numSentences = 0, tokenLocation = 0;
        boolean firstError = true;
        String prevLine = br.readLine();
        while ((curLine = br.readLine()) != null) {
            if (prevLine.equals("</s>")) {
                firstError = true;
                numSentences++;
                tokenLocation = 0;
            }

            // if there is a confusing word then calculate probability of both options
            if (confusingWords.get(curLine) != null) {
                int bigramFreq, bigramFreqAlt;
                try {
                    bigramFreq = bigramFrequencies.get(prevLine + separator + curLine);
                } catch (NullPointerException e) {
                    bigramFreq = 0;
                }
                try {
                    bigramFreqAlt = bigramFrequencies.get(prevLine + separator + confusingWords.get(curLine));
                } catch (NullPointerException e) {
                    bigramFreqAlt = 0;
                }

		double count1, count2;

		if(bigramFreq < 6) {
			count1 = GTVals.get(bigramFreq);
		} else {
		// get from ff.txt
		}
	
		if(bigramFreqAlt < 6) {
			count2 = GTVals.get(bigramFreqAlt);
		} else {
		// get from ff.txt
		}

/*
                double lap1 = (bigramFreq + 1.0) / (N + V);
                double lap2 = (bigramFreqAlt + 1.0) / (N + V);

                // if it's wrong then we need to output that
                if (lap1 < lap2) {
                    if (firstError) {
                        if (sb.length() > 0) {
                            sb.append("\n");
                        }
                        sb.append(numSentences).append(":");
                        firstError = false;
                    }
                    sb.append(tokenLocation).append(",");
                }
            }
*/
            tokenLocation++;
            prevLine = curLine;
        }

        // write information to file
        pw.write(sb.toString());
        pw.close();
    }

}
